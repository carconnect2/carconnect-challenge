import React, {useState} from "react";
import {Link} from "react-router-dom";
import LOCAL_HOST_PATH from "../constants";

type DataProps = {
  firstname: string;
  lastname: string;
  email: string;
  phoneNumber: string;
  postCode: string;
};

const HomePage = () => {
  // Form Initial State
  const formInitialState = {
    firstname: "",
    lastname: "",
    email: "",
    phoneNumber: "",
    postCode: "",
  };

  // States
  const [input, setInput] = useState<DataProps>(formInitialState);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [response, setResponse] = useState<any>({});

  const inputOnChangeHandler = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const {name, value} = e.target;
    setInput((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const onSubmitHandler = async (e: any) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const user = await fetch(`${LOCAL_HOST_PATH}api/user/registration`, {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify(input),
      });
      const responseData = await user.json();
      setResponse(responseData);

      if (responseData.success) {
        e.target.reset();
      }

      setTimeout(() => {
        setResponse("");
      }, 1500);
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="main-blk">
      <form onSubmit={onSubmitHandler} className="registration-blk">
        <h2>Registration</h2>
        <hr />
        <input
          type="text"
          name="firstname"
          placeholder="First Name"
          onChange={inputOnChangeHandler}
        />
        <input
          type="text"
          name="lastname"
          placeholder="Last Name"
          onChange={inputOnChangeHandler}
        />
        <input
          type="text"
          name="email"
          placeholder="Email"
          onChange={inputOnChangeHandler}
        />
        <input
          type="text"
          name="phoneNumber"
          placeholder="Phone Number"
          onChange={inputOnChangeHandler}
        />
        <input
          type="text"
          name="postCode"
          placeholder="Postal Code"
          onChange={inputOnChangeHandler}
        />
        <button>SUBMIT</button>
        {isLoading ? <h3>Loading</h3> : <h3>{response.message}</h3>}
      </form>
      <Link to="/users">Go to Users Page</Link>
    </div>
  );
};

export default HomePage;
