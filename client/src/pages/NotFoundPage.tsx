import React from "react";

const NotFoundPage = () => {
  return (
    <div className="main-blk">
      <h1>PAGE NOT FOUND</h1>
    </div>
  );
};

export default NotFoundPage;
