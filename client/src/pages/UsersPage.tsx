import {Link} from "react-router-dom";
import useFetch from "../hooks/useFetch";
import LOCAL_HOST_PATH from "../constants";

const UsersPage = () => {
  const {loading, data, error} = useFetch(`${LOCAL_HOST_PATH}api/users`);

  if (loading) {
    return <h3>Loading...</h3>;
  }

  if (error) {
    return <h3>Error Loading Data</h3>;
  }

  if (data?.users?.length === 0) {
    return (
      <div className="main-blk">
        <h1>No users on the database</h1>
        <Link to="/">Go back to Home</Link>
      </div>
    );
  }

  return (
    <div className="main-blk">
      {data?.users
        ?.map((user) => {
          return (
            <div className="user-card-blk" key={user._id}>
              <p>
                <b>Firstname:</b> {user.firstname}
              </p>
              <p>
                <b>Lastname:</b> {user.lastname}
              </p>
              <p>
                <b>Email:</b> {user.email}
              </p>
              <p>
                <b>Phone:</b> {user.phoneNumber}
              </p>
              <p>
                <b>Postal Code:</b> {user.postCode}
              </p>
            </div>
          );
        })
        .reverse()}
      <Link to="/">Go back to Home</Link>
    </div>
  );
};

export default UsersPage;
