import {useState, useEffect} from "react";

type DataProps = {
  message: string;
  users:
    | {
        _id: string;
        firstname: string;
        lastname: string;
        email: string;
        phoneNumber: string;
        postCode: string;
      }[]
    | null;
} | null;

const useFetch = (url: string) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [data, setData] = useState<DataProps>(null);
  const [error, setError] = useState<string>("");

  useEffect(() => {
    let apiSubscribed = true;
    const getData = async () => {
      try {
        const requestData = await fetch(url);
        const responseData = await requestData.json();
        if (apiSubscribed) {
          setData(responseData);
        }
      } catch (error) {
        setError("NOPE");
      } finally {
        setLoading(false);
      }
    };

    getData();

    return () => {
      apiSubscribed = false;
    };
  }, [url]);

  return {loading, data, error};
};

export default useFetch;
