import express from "express";
import {registerUser, getAllUsers} from "../controllers/user.controllers.js";

const router = express.Router();

// Show All Users
router.get("/users", getAllUsers);

// Add User
router.post("/user/registration", registerUser);

export default router;
