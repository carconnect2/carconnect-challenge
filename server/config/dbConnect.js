import mongoose from "mongoose";

const DBConnect = async () => {
  mongoose.connect(process.env.MONGODB_URI);
  const database = mongoose.connection;
  database.on("open", () => console.log("Database connected."));
  database.on("error", () => console.log("Database connection error."));
};

export default DBConnect;
