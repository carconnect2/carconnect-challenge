import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import DBConnect from "./config/dbConnect.js";
import userRoutes from "./routes/user.routes.js";

dotenv.config();
DBConnect();

const PORT = process.env.PORT || 5000;
const app = express();

// Declare express middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Cross-Origin Declaration
app.use(cors());

// User Routes
app.use("/api", userRoutes);

app.listen(PORT, async () => {
  console.log("Server is running...");
});
