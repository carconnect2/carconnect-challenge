import mongoose from "mongoose";

/* Fields: 
    - FirstName
    - LastName,
    - Email,
    - Phone number
    - Postcode
*/

const User = new mongoose.Schema({
  firstname: {
    type: String,
  },
  lastname: {
    type: String,
  },
  email: {
    type: String,
    lowercase: true,
    unique: true,
  },
  phoneNumber: {
    type: String,
  },
  postCode: {
    type: String,
  },
});

export default mongoose.model("users", User);
