import User from "../models/UserModel.js";

const getAllUsers = async (req, res) => {
  try {
    const users = await User.find();
    res.status(200).json({
      success: true,
      users,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: "Error retrieving data.",
    });
  }
};

const registerUser = async (req, res) => {
  const {firstname, lastname, email, phoneNumber, postCode} = req.body;

  try {
    if (![firstname, lastname, email, phoneNumber, postCode].every(Boolean)) {
      return res.status(401).json({
        success: false,
        message: "All fields are required.",
      });
    }

    const validateEmail = await User.findOne({email});
    if (validateEmail) {
      return res.status(401).json({
        success: false,
        message: "Email already exists. Try again.",
      });
    }
    const createUser = new User({
      firstname,
      lastname,
      email,
      phoneNumber,
      postCode,
    });

    await createUser.save();

    res.status(200).json({
      success: true,
      message: "The user has been successfully added.",
    });
  } catch (error) {
    console.log(error);
  }
};

export {getAllUsers, registerUser};
