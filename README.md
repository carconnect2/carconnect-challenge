## CarConnect Code Challenge

## .env

The 'MONGODB_URI' in the .env file is required in order to establish a connection with MongoDB.

## NodeJS and Express as the back-end / React and Typescript for the front-end

npm install in the root folder

npm install in the client folder

To execute the application, use the command: npm run dev

I've configured a test database on MongoDB Atlas to store and retrieve data.

Additionally, I've designed a page for viewing the stored information.

## Features that required further improvement

Form Validation (email, phone number and postal code)

I can further break down components into finer-grained elements in order to achieve an atomic design pattern.

I can also add a custom hook for POST requests
